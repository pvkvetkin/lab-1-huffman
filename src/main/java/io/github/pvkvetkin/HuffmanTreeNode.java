package io.github.pvkvetkin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
@AllArgsConstructor
public class HuffmanTreeNode implements Comparable<HuffmanTreeNode> {
    public final Character character;
    public final int frequency;
    public HuffmanTreeNode ch;
    public HuffmanTreeNode pr;

    public String getCodeForCharacter(Character ch, String parentPath) {
        if (character == ch) {
            return parentPath;
        } else {
            if (this.ch != null) {
                String path = this.ch.getCodeForCharacter(ch, parentPath + 0);
                if (path != null) {
                    return path;
                }
            }
            if (pr != null) return pr.getCodeForCharacter(ch, parentPath + 1);
        }
        return null;
    }

    @Override
    public int compareTo(HuffmanTreeNode o) {
        return Integer.compare(o.frequency, frequency);
    }
}