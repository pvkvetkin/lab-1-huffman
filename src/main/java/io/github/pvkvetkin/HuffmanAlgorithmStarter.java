package io.github.pvkvetkin;

import io.github.pvkvetkin.utils.DataParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class HuffmanAlgorithmStarter {

    // encoder/decoder infile/zipfile zipfile/infile
    public static void main(String[] args) {
        if (args.length < 3) {
            System.out.println("Usage: java -cp encode-and-decode-1.0-SNAPSHOT.jar io.github.pvkvetkin.HuffmanAlgorithmStarter encoder/decoder infile/zipfile zipfile/infile");
            return;
        }

        try {
            switch (args[0]) {
                case "encoder" -> {
                    String data = DataParser.parseFile(args[1]);

                    var frequencies = DataParser.computeCharacterFrequency(data);

                    var codeTreeNodes = DataParser.buildStartNodes(frequencies);

                    HuffmanTreeNode lastHuffmanTreeNode = DataParser.buildHuffmanTree(codeTreeNodes);

                    var codeWords = DataParser.getCodeOfCharacters(frequencies, lastHuffmanTreeNode);

                    String encodedData = DataParser.encodeData(data, codeWords);

//                    System.out.println("Размер исходной файла: " + data.getBytes().length + " байт");
//                    System.out.println("Размер сжатого файла: " + encodedData.length() / 8 + " байт");
//                    System.out.println("Процент сжатия: " + (1 - (double) encodedData.length() / (data.getBytes().length * 8)) * 100 + "%");
//                    System.out.println("Средняя затрата на символ: " + (double) encodedData.length() / data.length() + " бит");

                    DataParser.saveToFile(args[2], frequencies, encodedData);
                }
                case "decoder" -> {
                    Map<Character, Integer> frequencies = new HashMap<>();
                    StringBuilder sb = new StringBuilder();

                    DataParser.loadFromFile(args[1], frequencies, sb);

                    var codeTreeNodes = DataParser.buildStartNodes(frequencies);

                    HuffmanTreeNode lastHuffmanTreeNode = DataParser.buildHuffmanTree(codeTreeNodes);

                    String decodedData = DataParser.decodeHuffmanTree(sb.toString(), lastHuffmanTreeNode);

                    DataParser.saveDataInFile(args[2], decodedData);
                }
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
