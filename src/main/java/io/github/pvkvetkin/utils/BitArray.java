package io.github.pvkvetkin.utils;

import lombok.Getter;

import java.util.BitSet;

public class BitArray {
    @Getter
    private final int size;
    private BitSet bits;

    public BitArray(int size) {
        this.size = size;
        this.bits = new BitSet(size);
    }

    public BitArray(int size, byte[] bytes) {
        this.size = size;
        this.bits = BitSet.valueOf(bytes);
    }

    public int get(int index) {
        return bits.get(index) ? 1 : 0;
    }

    public void set(int index, int value) {
        if (value != 0) {
            bits.set(index);
        } else {
            bits.clear(index);
        }
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append(bits.get(i) ? '1' : '0');
        }
        return sb.toString();
    }

    public int getSizeInBytes() {
        return (int) Math.ceil(size / 8.0);
    }

    public byte[] getBytes() {
        return bits.toByteArray();
    }
}
