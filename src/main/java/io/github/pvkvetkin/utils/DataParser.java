package io.github.pvkvetkin.utils;

import io.github.pvkvetkin.HuffmanTreeNode;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class DataParser {

    public static String parseFile(String pathToFile) throws IOException {
//        var bytes = Files.readAllBytes(Paths.get(pathToFile));
//        return new String(bytes);
        byte[] bytes;
        try (var inputStream = Files.newInputStream(Paths.get(pathToFile))) {
            bytes = inputStream.readAllBytes();
        }
        return new String(bytes);
    }

    public static void saveDataInFile(String pathToFile, String data) throws IOException {
        try (var outputStream = Files.newOutputStream(Paths.get(pathToFile))) {
            outputStream.write(data.getBytes());
        }
    }

    public static void saveToFile(String pathToFile, Map<Character, Integer> frequencies, String bits) throws IOException {
        try (DataOutputStream os = new DataOutputStream(new BufferedOutputStream(Files.newOutputStream(Paths.get(pathToFile))))) {
            os.writeInt(frequencies.size());
            for (Map.Entry<Character, Integer> entry : frequencies.entrySet()) {
                os.writeChar(entry.getKey());
                os.writeInt(entry.getValue());
            }

            BitArray bitArray = new BitArray(bits.length());
            for (int i = 0; i < bits.length(); i++) {
                bitArray.set(i, bits.charAt(i) != '0' ? 1 : 0);
            }

            os.writeInt(bits.length());
            os.write(bitArray.getBytes(), 0, bitArray.getSizeInBytes());
        }
    }

    public static void loadFromFile(String pathToFile, Map<Character, Integer> frequencies, StringBuilder bits) throws IOException {
        try (DataInputStream os = new DataInputStream(new BufferedInputStream(Files.newInputStream(Paths.get(pathToFile))))) {
            int frequencyTableSize = os.readInt();
            for (int i = 0; i < frequencyTableSize; i++) {
                frequencies.put(os.readChar(), os.readInt());
            }

            int dataSizeBits = os.readInt();
            int dataSizeBytes = (int) Math.ceil(dataSizeBits / 8.0);

            byte[] dataBytes = new byte[dataSizeBytes];
            os.readFully(dataBytes);

            BitArray bitArray = new BitArray(dataSizeBits, dataBytes);

            for (int i = 0; i < bitArray.getSize(); i++) {
                bits.append(bitArray.get(i) != 0 ? "1" : "0");
            }
        }
    }


    public static Map<Character, Integer> computeCharacterFrequency(String text) {
        return text.chars().boxed()
                .collect(Collectors.toMap(
                        c -> (char) c.intValue(),
                        c -> 1,
                        Integer::sum
                ));
    }

    public static List<HuffmanTreeNode> buildStartNodes(Map<Character, Integer> frequencies) {
        return frequencies.entrySet().stream()
                .map(entry -> new HuffmanTreeNode(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());
    }

    public static Map<Character, String> getCodeOfCharacters(Map<Character, Integer> frequencies, HuffmanTreeNode lastHuffmanTreeNode) {
        return frequencies.keySet().stream()
                .collect(Collectors.toMap(
                        c -> c,
                        c -> lastHuffmanTreeNode.getCodeForCharacter(c, ""),
                        (existing, replacement) -> existing,
                        LinkedHashMap::new
                ));
    }

    public static HuffmanTreeNode buildHuffmanTree(List<HuffmanTreeNode> nodes) {
        while (nodes.size() > 1) {
            nodes.sort(Comparator.naturalOrder());
            HuffmanTreeNode ch = nodes.remove(nodes.size() - 1);
            HuffmanTreeNode pr = nodes.remove(nodes.size() - 1);
            nodes.add(new HuffmanTreeNode(null, pr.frequency + ch.frequency, ch, pr));
        }
        return nodes.get(0);
    }


    public static String encodeData(String text, Map<Character, String> codeWords) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); i++) {
            sb.append(codeWords.get(text.charAt(i)));
        }
        return sb.toString();
    }

    public static String decodeHuffmanTree(String encodedData, HuffmanTreeNode endNodeTree) {
        StringBuilder sb = new StringBuilder();
        HuffmanTreeNode node = endNodeTree;
        for (int i = 0; i < encodedData.length(); i++) {
            node = encodedData.charAt(i) == '0' ? node.ch : node.pr;
            if (node.character != null) {
                sb.append(node.character);
                node = endNodeTree;
            }
        }
        return sb.toString();
    }
}
